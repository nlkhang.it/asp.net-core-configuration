﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DatabaseConnectionString
{
    public class EfContext1 : DbContext
    {
        private readonly string _connectionString;

        protected EfContext1() : base()
        {
            var buider = new ConfigurationBuilder();
            buider.AddJsonFile("appsettings.json", false);

            var configuration = buider.Build();
            _connectionString = configuration.GetConnectionString("SQLConnection");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }
    }
}
